var nav = {
    nav:null,
    section:null,
    open:false,
    configuration(navId,sectionId,barsId){
                this.nav  =  helper.id(navId) ;
                this.section = helper.id(sectionId) ;
                this.bars = helper.id(barsId);
                this.bars.addEventListener('click',() => {
                          this.openMenu();
                });
                this.bars.addEventListener('mouseover',() => {
                          this.openMenu();
                });
    },
    // This Method set the MarginLeft of Nav Tag = 0
    //and Section marginLeft == Nav Width
    openMenu(){
        console.log(this)
        //this  = barsId
                if(this.open){
                    this.nav.style.marginLeft = "-250px";
                    this.section.style.marginLeft = "0px"; 
                    this.open = false;
                } else{
                    this.nav.style.marginLeft = "0px";
                    this.section.style.marginLeft = "250px"; 
                    this.open = true;
                }
    }  ,
}

